/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package array.practice;


/**
 *
 * @author Mitchel
 */


public class ArrayPractice {
    
   

    public static void main(String[] args) {
        
        int[] arr;
        arr = new int[5];
        arr[0] = 78;
        arr[1] = 84;
        arr[2] = 92;
        arr[3] = 65;
        arr[4] = 89;
        
        int[] cloneArr = arr.clone();

        System.out.println("Initial array: ");
        for (int i = 0; i < arr.length; i++){
        System.out.println(arr[i]);
        }
        sort(arr);
        insert(arr, 78, 100);
        int kSmall = kSmallest(arr, 3);
        System.out.println("3rd smallest: " + kSmall);
        
        int[][] sideBySide;
        sideBySide = new int[2][5];
        for (int i = 0; i < 5; i++){
            sideBySide[0][i] = arr[i];
            sideBySide[1][i] = cloneArr[i];
        }
        
        System.out.println("Original array: ");
        for (int i = 0; i < 5; i++){
        System.out.println(sideBySide[1][i]);
        }
    }
    
    public static void insert(int[] array, int x, int y){
        int find = x;
        int insert = y;
        System.out.println("Insert");
        for(int i = 0; i < array.length; i++){
            if (array[i] == find){
                array[i] = insert;
            }
            System.out.println(""+array[i]);
        }
        
    }
    
    public static int kSmallest(int[] array, int k){
        sort(array);
        return array[k-1];
    }
    
    public static void sort(int[] array){
            System.out.println("Sort");
            for (int lh = 0; lh < array.length; lh++){
                int rh = findSmallest(array, lh, array.length);
                swapElements(array, lh, rh);
                System.out.println(""+array[lh]);
            }
            
        }
        
    public static int findSmallest(int[] array, int p1, int p2){
        int smallestIndex = p1;
        for(int i = p1 + 1; i < p2; i++){
            if (array[i] < array[smallestIndex]){
                smallestIndex= i;
            }
        }
        return smallestIndex;
    }
    
    public static void swapElements(int[] array, int p1, int p2){
        int temp= array[p1];
        array[p1] = array[p2];
        array[p2] = temp;
    }
        

    }
    