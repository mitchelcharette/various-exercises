/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arraylistpractice;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Mitchel
 */
public class ArrayListPractice {

    
    public static void main(String[] args) {
        
        int[] arr;
        arr = new int[5];
        arr[0] = 78;
        arr[1] = 84;
        arr[2] = 92;
        arr[3] = 65;
        arr[4] = 89;
        
        
        List<String> faculty = new ArrayList(21);
        faculty.add("Isaac");
        
        List peopleInClass = new ArrayList(faculty);
        peopleInClass.add("Mitchel");
        peopleInClass.add("Justin");
        peopleInClass.add("Angel");
        peopleInClass.add("Tait");
        peopleInClass.add("Malachai");
        peopleInClass.add("Will");
        peopleInClass.add("Anne");
        peopleInClass.add("Nahuel");
        peopleInClass.add("Gabriel");
        peopleInClass.add(20);
        peopleInClass.add(arr);
        peopleInClass.add(5, "Malachi");
        peopleInClass.remove("Malachai");
        
        System.out.println(faculty);
        System.out.println(peopleInClass);
        
    }
    
}
