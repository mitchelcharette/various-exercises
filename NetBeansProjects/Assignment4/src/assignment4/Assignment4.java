/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment4;


public class Assignment4 {

    Node head;
static class Node{
    String data;
    Node next;
    Node(String d){
        data = d;
        next = null;
    }
    

}

public void printList(){
    Node var = head;
    while(var != null){
        System.out.print(var.data+", ");
        var = var.next;
    }
    System.out.println();
}

    public void push(String new_data){
        Node new_node = new Node(new_data);
        new_node.next = head;
        head = new_node;
    }
    
    public void insertAfter(Node prev_node, String new_data){
       if(prev_node == null) {
           System.out.println("the given previous node cannot be null.");
       }else{
       Node new_node = new Node(new_data);
       new_node.next = prev_node.next;
       prev_node.next= new_node;
    }
    }
    
public boolean search(String key){
            Node temp = head;
            
            while(temp != null && temp.data != key){
                temp = temp.next;
            }
            
            if(temp != null){
                return true;
            }
            else{
                return false;
    }
        }
        
        public int count(){
            Node n = head;
            int num = 0;
            while(n != null){
                n = n.next;
                num++;
            }
            return num;
        }
        
        public int countKey(String key){
            int num = 0;
            Node x = head;
            while(x != null){
                if(x.data == key){
                    num++;
                }
            x = x.next;    
            }
            return num;
        }
         
        
        Node reverse(Node node){
            
            Node prev = null;
            Node curr = node;
            Node next = null;
            while(curr != null){
                next = curr.next;
                curr.next = prev;
                prev = curr;
                curr = next;
            }
            node = prev;
            return node;
        }
        
        public void lastToFirst(){
            Node last = head;
            Node prev = null;
            while(last.next != null){
                prev = last;
                last = last.next;
            }
            prev.next = null;
            last.next = head;
            head = last;
        }
        
        public void swap(Node prevX, Node prevY){
            
                Node x = null;
                Node y = null;
                if(prevX == null){
                    System.out.println("Do not use input of linked list");
                    return;
            }
                if(prevY == null){
                    System.out.println("Do not use input of linked list");
                    return;
                }
                if(prevY == prevX){
                    System.out.println("Use two different nodes");
                    return;
                }
                x = prevX.next;
                y = prevY.next;
                
                Node temp = x.next;
                
                x.next = y.next;
                y.next = temp;
                
                prevX.next=y;
                prevY.next=x;
                
        }
    
    public void append(String new_data){
        Node new_node = new Node(new_data);
        
        if (head == null){
            head = new Node(new_data);
        }else{
        new_node.next = null;
        Node last = head; 
        while(last.next != null){
            last = last.next;
        }
        last.next = new_node;
    }
    }
    
    public void deleteNode(String key){
        Node temp = head, prev = null;
        
        //for when key is the head
        if(temp != null && temp.data == key){
            head = temp.next;
            return;
        }
        
        //finds the key in the linked list
        while(temp != null && temp.data != key){
            prev = temp;
            temp = temp.next;
        }
        
        //if key is not in linked list returns error
        if(temp == null){
            System.out.println("Your key was not found in the linked list");
            return;
        }
        
        //deletes key
        prev.next = temp.next;
        
    }
    
    void deleteList(){
        head = null;
    }
    

    
    public static void main(String[] args) {
        Assignment4 MathTerms = new Assignment4();
        MathTerms.head= new Node("Calculus");
        Node second = new Node("Algebra");
        Node third = new Node("Differential");
        Node fourth = new Node("Variable");
        Node fifth = new Node("Integral");
        Node sixth = new Node("Derivative");
        Node seventh = new Node("Geometry");
        Node eighth = new Node("Multiplication");
        Node ninth = new Node("Subtraction");
        Node tenth = new Node("Division");
        Node eleventh = new Node("Subtraction");
        Node twelth =  new Node("Calculus");
        Node thirteenth = new Node("Summation");
        Node fourteenth = new Node("Factorial");
        Node fifteenth = new Node("Combination");
        Node sixteenth = new Node("Relations");
        Node seventeenth = new Node("Partitons");
        Node eighteenth = new Node("Exponents");
        Node nineteenth = new Node("Rationals");
        Node twentieth = new Node("Maths");
        
        MathTerms.head.next = second;
        second.next = third;
        third.next = fourth;
        fourth.next = fifth;
        fifth.next = sixth;
        sixth.next = seventh;
        seventh.next = eighth;
        eighth.next =ninth;
        ninth.next = tenth;
        tenth.next = eleventh;
        eleventh.next = twelth;
        twelth.next = thirteenth;
        thirteenth.next = fourteenth;
        fourteenth.next = fifteenth;
        fifteenth.next = sixteenth;
        sixteenth.next = seventeenth;
        seventeenth.next = eighteenth;
        eighteenth.next = nineteenth;
        nineteenth.next = twentieth;
        
        MathTerms.printList();
        System.out.println();
        if(MathTerms.search("Maths")){
        System.out.println("Maths was found.");
        }else{
            System.out.println("Maths was not found.");
        }
        System.out.println("Number of elements: "+MathTerms.count());
        System.out.println("Alebra shows up: "+MathTerms.countKey("Algebra"));
        if(MathTerms.search("Mitchel")){
            System.out.println("Term was found by search method.");
        }
        else{
            System.out.println("Term was not found in linked list.");
        }
        MathTerms.lastToFirst();
        System.out.println("Last is now first. ");
        MathTerms.printList();
        int x = MathTerms.count();
        System.out.println("Length of linked list: "+x);
        
        int y = MathTerms.countKey("Calculus");
        System.out.println("Number of times Calculus appears: "+y);
        MathTerms.swap(third, ninth);
        MathTerms.deleteNode("Summation");
        System.out.println("Swapped the thrid and tenth modes and deleted summation.");
        MathTerms.printList();
        MathTerms.head = MathTerms.reverse(MathTerms.head);
        System.out.println("Reveresed terms: ");
        MathTerms.printList();
        MathTerms.push("Commutativity");
        MathTerms.insertAfter(eleventh, "Associativity");
        System.out.println("List with Associativity and commutativity added. ");
        MathTerms.printList();
        MathTerms.deleteList();
        System.out.println("Deleted the list.");
        MathTerms.printList();
    
    }
         
}
