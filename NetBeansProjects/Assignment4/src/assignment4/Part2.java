/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment4;
import java.util.*;

public class Part2 {
    
    public void addStuff(LinkedList<String> x){
        x.add("Cucumbers");
        x.add("To my toes");
        x.add("Potatoes");
        x.add("Carrots");
        x.add("Peas");
        x.add(2, "Greenbeans");
        x.addFirst("Tomatoes");
    }
    
    public void removeStuff(LinkedList<String> x){
        x.remove("To my toes");
        x.remove(3);
        x.removeFirst();
    }
    
    public static void main(String[] args){
        
        LinkedList<String> object = new LinkedList<>();
        Part2 t = new Part2();
        t.addStuff(object);
        System.out.println(object);
        t.removeStuff(object);
        if(object.contains("Cucumbers")){
            int y = object.indexOf("Cucumbers");
                    System.out.println("I love "+object.get(y)+"!");
        }
        System.out.println("There are "+object.size()+" vegetables that I love "
                + "a lot");
        System.out.println(object);
        object.clear();
        
    }
}
