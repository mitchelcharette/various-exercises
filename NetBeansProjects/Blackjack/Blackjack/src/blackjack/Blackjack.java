/*
 * This program is a representation of the card game Blackjack on a video interface.
 * (There should be more description here)
 */
package blackjack;

import acm.graphics.GLabel;
import acm.program.GraphicsProgram;
import static acm.program.Program.SOUTH;
import acm.util.RandomGenerator;
import java.awt.Color;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import svu.csc213.Dialog;

/**
 *
 * @author Kyle York
 */
public class Blackjack extends GraphicsProgram {
    
    private JButton wagerButton;
    private JButton exitButton;
    private JButton hitButton;
    private GLabel wagerLabel;
    private GLabel balanceLabel;
    private JButton stayButton;
    private JButton dealButton;
    private int balance = 100;
    private int wager = 10;
    private Deck deck= new Deck();
    private GHand dealer= new GHand(deck);
    private GHand player= new GHand(deck);
    private int playerPoints = player.getValue();
    private int dealerPoints = dealer.getValue();
    private Hand player2 = new Hand(deck);
    private GLabel cardLabel = new GLabel("Cards 2-9 pts equals the value on card.");
    private GLabel tenLabel = new GLabel("Cards 10-king each equal 10pts.");
    private GLabel aceLabel = new GLabel("The ace is worth either 1 or 11.");
    private GLabel directions = new GLabel("You by having a higher value than the dealer without breaking 21.");
    
    
    @Override
    public void init() {
        // This sets up the initial game screen
       setBackground(new Color(80,170,90));
    
    wagerButton = new JButton("Wager");
    add(wagerButton, SOUTH);
    dealButton = new JButton("Deal");
    add(dealButton, SOUTH);
    hitButton = new JButton("Hit");
    add(hitButton, SOUTH);
    hitButton.setEnabled(false);
    stayButton = new JButton("Stay");
    add(stayButton, SOUTH);
    stayButton.setEnabled(false);
    exitButton = new JButton("Exit");
    add(exitButton, SOUTH);
    wagerLabel = new GLabel("Wager: $" + wager);
    wagerLabel.setFont("times-20");
    add(wagerLabel, 20, getHeight() - wagerLabel.getHeight()*1.5-20);
    balanceLabel = new GLabel("Balance: $" + balance);
    balanceLabel.setFont("times-20");
    add(balanceLabel, getWidth()-20-balanceLabel.getWidth()-20, getHeight()-balanceLabel.getHeight()*1.5-20);
    addActionListeners();
    
    cardLabel.setFont("times-16");
    tenLabel.setFont("times-16");
    aceLabel.setFont("times-16");
    directions.setFont("times-16");
    add(cardLabel, 20, 30);
    add(tenLabel, 20, 50);
    add(aceLabel, 20, 70);
    add(directions, 20,90);
    
    }
    
    @Override
    public void run() {

    }
    
    public void wager(){
        // This allows the player to chang ehow much they will wager
        boolean goodWager = false;
        while(!goodWager){
            int newWager = Dialog.getInteger(this, "Enter new wager?");
            if (newWager<1 || newWager>balance){
                Dialog.showMessage(this, "Please enter a wager between 1 and" + balance);
            }else{
                wager = newWager;
                wagerLabel.setLabel("Wager: $" + wager);
                goodWager = true;
            }
        }
    }
    
    
    private void deal(){
        // This will deal out a set of 2 cards for both the player and dealer and set one of the dealer's cards face down
        cardLabel.setVisible(false);
        tenLabel.setVisible(false);
        aceLabel.setVisible(false);
        directions.setVisible(false);
        dealButton.setEnabled(false);
        wagerButton.setEnabled(false);
        stayButton.setEnabled(true);
        hitButton.setEnabled(true);
        this.update(this.getGraphics());
        add(player,20,250);
        add(dealer,20,80);
        player.getGCard(0).turnOver();
        player.getGCard(1).turnOver();
        dealer.getGCard(0).turnOver();
        playerPoints = player.getValue();
    }
    
    
    private void hit(){
        // Adds another card to the player's hand
        player.hit();
        playerPoints = player.getValue();
        if(playerPoints>=22){
            playerPoints = 0;
            stay();
        }else{
            playerPoints = player.getValue();
        
        }
    }
    

   
    private void stay(){
         // The player keeps the cards they have in their hand and then the dealer plays
        hitButton.setEnabled(false);
        stayButton.setEnabled(false);
        dealer.getGCard(1).turnOver();
        pause(1000);
        this.update(this.getGraphics());
        dealerPoints = dealer.getValue();
        while(dealerPoints<16 && dealerPoints!=0){
        if(dealerPoints<16){
            dealer.hit();
            pause(1000);
            dealerPoints = dealer.getValue();
            if(dealerPoints>21){
                dealerPoints = 0;
                finish();
            }
        } 
    }
        if(dealerPoints>=16 && dealerPoints<=21){
            dealerPoints = dealer.getValue();
            finish();
            
        }
    }
    
    
    private void finish() {
        //Totals the scores and makes changes on the balance based on if the player won, lost, or tied
        if(dealerPoints>playerPoints || playerPoints==0){
            balance-=wager;
            Dialog.showMessage("You lose.");
        }else if(dealerPoints<playerPoints){
            balance+=wager;
            Dialog.showMessage("You win.");
        } else if(dealerPoints == playerPoints && dealerPoints!=0){
            Dialog.showMessage("You tied, no money lost.");
        }
        balanceLabel.setLabel("Balance: $ " + balance);
        wagerButton.setEnabled(true);
        dealButton.setEnabled(true);
        if(wager>balance && balance!=0){
            wager = balance;
            balanceLabel.setLabel("Balance: $ " + balance);
            wagerLabel.setLabel("Wager: $ " + wager);
            Dialog.showMessage("Your wager has been lowered. If you wish to change it use the \"wager\" button'");
        }
        if(balance == 0){
            Dialog.showMessage("You are all out of cash! Here, have some on the house!");
            balance = 50;
            balanceLabel.setLabel("Balance: $ " + balance);
        }
        remove(player);
        remove(dealer);
        player = new GHand(deck);
        dealer = new GHand(deck);

        this.update(this.getGraphics());
    }
   
    @Override
    public void exit(){
         // Exits the simulation
        if(Dialog.getYesOrNo("Are you sure you want to quit?")==true){
        System.exit(0);
        }
    }

    
    
    @Override
    public void actionPerformed(ActionEvent ae){
        // The actions of each of the buttons
        switch(ae.getActionCommand()){
            case "Wager":
                wager(); break;
            case "Stay":
                stay(); break;
            case "Deal":
                deal(); break;
            case "Hit":
                hit(); break;
            case "Exit":
                exit(); break;
            default:
                //Dialog.showMessage(this,"Something else.");
                        
        }
    }
    

   
    public static void main(String[] args) {
        new Blackjack().start();
    }

    
    
}
