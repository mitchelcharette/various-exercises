/*
 * Creates a random card
 */
package blackjack;

/**
 *
 * @author student
 */
public class Card {
    
    
    public enum Suit{
        //The set of suits in a deck of cards
        CLUBS, DIAMONDS, HEARTS, SPADES
    }
    
    
    public enum Face{
        //The set of face values in a deck of cards
        ACE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING
    }
     
    private Suit suit;
    private Face face;        
    
    
    public Card(Suit suit, Face face){
        //Assigns a suit and a value for each card
        this.suit = suit;
        this.face = face;
    }

    
    public Suit getSuit() {
        return suit;
    }

    public Face getFace() {
        return face;
    }
    public boolean isAce() {
        return face==Face.ACE;
    }
    
    
   
    public int getValue() {
        //Used to get value of a card
        if (face.compareTo(Face.TWO)>=0 && face.compareTo(Face.TEN)<=0) {
            return face.ordinal()+1;
        }

        if (face.compareTo(Face.JACK)>=0 && face.compareTo(Face.KING)<=0){
            return 10;
        }
        return 11;
    }
    
    @Override
    public String toString(){
        return face + " OF " + suit;
    }
}
