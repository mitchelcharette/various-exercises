/*
 * Creates a deck that holds the cards and their order, shuffling cards.
 * 
 */
package blackjack;

import acm.util.RandomGenerator;

/**
 *
 * @author student
 */
public class Deck {
    
    private Card[] cards = new Card[52];
    private int top;
    private Card[] temp= new Card[52];
    
    
    
    public Deck() {
        //A shuffled 52 card deck
        int index = 0;
        for(Card.Suit s: Card.Suit.values()){
            for(Card.Face f : Card.Face.values()){
                cards[index++] = new Card (s,f);
            }
        }
        shuffle();
    }
    
    
    private void shuffle() {
        // randomize position of the cards in the deck
        RandomGenerator gen = RandomGenerator.getInstance();
        for(int i=0; i<52; i++){
        int switchSpot = gen.nextInt(0,51);
        temp[i] = cards[i];
        cards[i]=cards[switchSpot];
        cards[switchSpot]=temp[i];
        }
        top = 0;

    }
    
    public Card deal() {
        return cards[top++];
    }
    
    @Override
    public String toString() {
       StringBuilder sb = new StringBuilder("[");
       for(Card card : cards) {
           sb.append(card);
           sb.append(",");
       }
       sb.append("]");
       return sb.toString();
    }
    
}
