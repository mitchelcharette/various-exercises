// This class creates a graphical image of a card
package blackjack;

import acm.graphics.GCompound;
import acm.graphics.GImage;
import acm.graphics.GRect;
import java.awt.Color;

/**
 *
 * @author student
 */
public class GCard extends GCompound {
    
    private Card card;
    private boolean faceUp;
    private GImage image;
    private GRect cardBack;
    
    public GCard(Card card){
        //Creates a graphic card that acts like the class Card
        this.card = card;
        faceUp = false;
        //The path to an imported set of card images
        String path = "cardgifs/" + 
                card.getSuit().toString().substring(0,1).toLowerCase() + 
                getImageFileNumber() + 
                ".gif";
        System.out.println(path);
        image = new GImage(path);
        GRect border = new GRect(109,152);
        cardBack = new GRect(107,150);
        cardBack.setFillColor(Color.blue);
        cardBack.setFilled(true);
        add(border);
        add(image,1,1);
        add(cardBack,1,1);
        this.scale(.78);
    }
    
    
    private int getImageFileNumber(){
        switch(card.getFace()){
            case ACE: return 14;
            default: return card.getFace().ordinal()+1;
            
        }
    }
    
    
    public boolean isAce(){
        //Used to determine if a card is and ace
        return card.isAce();
    }
    
    public int getValue(){
        return card.getValue();
    }
    
    public boolean isFaceUp(){
        return faceUp;
    }
    
    public void turnOver(){
        //"Flips" the card over
        faceUp = !faceUp;
        cardBack.setVisible(!faceUp);
    }
    
}
