//Creates a graphical representation of a hand and acts like the class Hand
package blackjack;

import acm.graphics.GCompound;
import java.util.ArrayList;

/**
 *
 * @author student
 */
public class GHand extends GCompound implements Comparable<GHand>{
    
    private Hand hand;
    private ArrayList<GCard> gcards = new ArrayList<>();
    
    public GHand(Deck deck){
        this(new Hand(deck));
    }
    
    public GHand(Hand hand){
        this.hand = hand;
        for(int i=0;i<hand.getSize();++i){
            GCard gcard= new GCard(hand.getCard(i));
            add(gcard,(gcard.getWidth()+20)*i,0);
            gcards.add(gcard);
        }
    }
    
    public void hit(){
        hand.hit();
        GCard gcard = new GCard(hand.getCard(hand.getSize()-1));
        gcard.turnOver();
        add(gcard,(gcard.getWidth()+20)*(hand.getSize()-1),0);
        gcards.add(gcard);
    }
    
    public int getValue(){
        return hand.getValue();
    }

    
    public GCard getGCard(int index){
        return gcards.get(index);
    }
    
   

    @Override
    public int compareTo(GHand o) {
        return hand.compareTo(o.hand);
    }
    
    
    
}
