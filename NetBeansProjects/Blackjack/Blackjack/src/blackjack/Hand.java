/*
 * This is a collection of Cards.
 * Compare hands and clear them.
 * Use array list
 */
package blackjack;

import java.util.ArrayList;

/**
 *
 * @author student
 */
public class Hand implements Comparable<Hand> {
    
    private ArrayList<Card> cards = new ArrayList<>();
    private Deck deck;
    
    public Hand(Deck deck){
        this.deck = deck;
        cards.add(deck.deal());
        cards.add(deck.deal());
    }
    
    public int getValue(){
        //Gets the total value of a Hand
        int total = 0;
        int aceCount = 0;
        for(Card card : cards){
            total+= card.getValue();
            if(card.isAce()){
                aceCount++;
            }
            while(total>21 && aceCount>0){
                total-=10;
                --aceCount;
            }
        }
    return total;
    }
    
    public int getSize(){
        return cards.size();
    }
    
    public Card getCard(int index){
    return cards.get(index);
}
    public void hit(){
        cards.add(deck.deal());
    }
    
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder("[");
        for(Card c : cards){
            sb.append(c);
        }
        sb.append("]");
        return sb.toString();
    }

    @Override
    public int compareTo(Hand o) {
        return getValue()-o.getValue();
    }
    
}

