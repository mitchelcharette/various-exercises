/*
 * This program is a representation of the card game Blackjack on a video interface.
 * (There should be more description here)
 */
package blackjack;

import acm.program.GraphicsProgram;

/**
 *
 * @author Mitchel Charette
 */
public class Blackjack extends GraphicsProgram {
    
    public void init() {
        
    }
    
    public void run() {
        Deck deck = new Deck();
        println(deck);
    }

   
    public static void main(String[] args) {
        new Blackjack().start();
    }
    
}
