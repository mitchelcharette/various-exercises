/*
 * Creates a random card
 */
package blackjack;

/**
 *
 * @author student
 */
public class Card {
    
    public static final int CLUBS = 0;
    public static final int DIAMONDS = 1;
    public static final int HEARTS = 2;
    public static final int SPADES = 3;
    
    public static final String [] suits ={"Clubs","Diamonds","Hearts","Spades"};
    
    public static final int ACE = 1;
    public static final int TWO = 2;
    public static final int THREE = 3;
    public static final int FOUR = 4;
    public static final int FIVE = 5;
    public static final int SIX = 6;
    public static final int SEVEN = 7;
    public static final int EIGHT = 8;
    public static final int NINE = 9;
    public static final int TEN = 10;
    public static final int JACK = 11;
    public static final int QUEEN = 12;
    public static final int KING = 13;
    
    public static final String [] faces = {"Ace", "Two", "Three", "Four",
     "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"};
    
    
    private int suit;
    private int face;        
    
    public Card(int suit, int face){
        this.suit = suit;
        this.face = face;
    }

    public int getSuit() {
        return suit;
    }

    public int getFace() {
        return face;
    }
    public boolean isAce() {
        return face==ACE;
    }
    
    public int getValue() {
        if (face<11 && face>1) {
            return face;
        }

        if (face>10){
            return 10;
        }
        return 11;
    }
    
    @Override
    public String toString(){
        return faces[face-1] + " of " + suits[suit];
    }
}
