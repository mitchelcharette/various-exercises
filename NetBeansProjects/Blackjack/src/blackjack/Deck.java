/*
 * Holding the cards and their order, shuffling cards.
 * 
 */
package blackjack;

/**
 *
 * @author student
 */
public class Deck {
    
    private Card[] cards;
    private int top;
    
    public Deck() {
        cards = new Card[52];
        int index = 0;
        for(int s = Card.CLUBS; s<=Card.SPADES;++s){
            for(int f = Card.ACE; f<=Card.KING;++f){
                cards[index++] = new Card (s,f);
            }
        }
    }
    
    private void shuffle() {
        // randomize position of the cards in the deck
        top = 0;
        
    }
    
    public Card deal() {
        return cards[top++];
    }
    
    @Override
    public String toString() {
       StringBuilder sb = new StringBuilder("[");
       for(Card card : cards) {
           sb.append(card);
           sb.append(",");
       }
       sb.append("]");
       return sb.toString();
    }
    
}
