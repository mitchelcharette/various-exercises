/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbms_connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
/**
 *
 * @author Mitchel
 */
public class DBMS_connection {

     public void Test_connection(String db_path, String username, String password)
    {
        try
        {
            Class.forName("com.mysql.cj.jdbc.Driver"); // new Driver
            Connection db_conx = null;
            db_conx = DriverManager.getConnection(db_path, username, password);
            
            //User Feedback
            if(db_conx != null)
                System.out.println("Database successfully connected !");
        }
        catch (SQLException es) //system handeling errors
        {
            System.err.format("SQL State: ", es.getSQLState());
            System.err.format("\n SQLException message: " + es.getMessage());
            System.err.format("\n VendorError: "+es.getErrorCode());
        }
        catch(Exception est)
                {
                 est.printStackTrace();  
                }
      
    }
     
     public void Create_schema(String newSchema)
     {
         Statement query_command = null;
         try{
             //execute query
             query_command = db_conx.createStatement();
             String query = "CREATE DATABASE IF NOT EXISTS " + newSchema;
             query_command.executeUpdate(query);
             System.out.println("\n Schema Successfully Created !");
         }
         catch(SQLException se)
         {
             se.printStackTrace();
         }
         catch(Exception e)
         {
             e.printStackTrace();
         }
     }
    
    public static void main(String[] args)
    {
        DBMS_connection dc = new DBMS_connection();
        
        String db_path = "jdbc:mysql://localhost/university";
        String username = "root";
        String password = "";
        
        dc.Test_connection(db_path, username, password);
    }
    
}
