/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package maze;

import acm.graphics.GLine;
import acm.program.GraphicsProgram;
import svu.csc213.Dialog;

/**
 *
 * @author Mitchel
 */
public class Maze extends GraphicsProgram {
  
    private int mazeHeight;
    private int mazeWidth;
    private int wallLength;
    private MazeWall mazeWall;
    private int leftBound;
    private int rightBound;
    private int topBound;
    private int bottomBound;
    
    @Override
    public void run(){
         
    mazeHeight = Dialog.getInteger(this,"How high do you want the maze to be?");
    
    mazeWidth = Dialog.getInteger(this,"How wide do you want the maze to be?");
    
        wallLength = 50;
    if(mazeHeight>=10 || mazeWidth>=10){
        wallLength = 30;
    }
    rightBound = this.getWidth()/2+mazeWidth*wallLength/2;
    leftBound = this.getWidth()/2-mazeWidth*wallLength/2;
    topBound = this.getHeight()/2-mazeHeight*wallLength/2;
    bottomBound = this.getHeight()/2+mazeHeight*wallLength/2;
    drawMaze();
    }
    
public void drawMaze(){
    
    for(int row=0; row<=mazeHeight; row++){
        for(int col=0; col<mazeWidth; col++){
            mazeWall = new MazeWall(leftBound+col*wallLength,topBound+row*wallLength,
                    leftBound+(col+1)*wallLength,topBound+row*wallLength);
            add(mazeWall);
        }
    }
     
}  
public void drawFrame(){
    
    GLine line1 = new GLine(leftBound, topBound, rightBound, topBound);
    GLine line2 = new GLine(rightBound, topBound, rightBound, bottomBound);
    GLine line3 = new GLine(rightBound, bottomBound, leftBound, bottomBound);
    GLine line4 = new GLine(leftBound, topBound, leftBound, bottomBound);

    add(line1);
    add(line2);
    add(line3);
    add(line4);
}

    
    public static void main(String[] args) {
        new Maze().start();
    }
    
}

