
package dicegame;

/**
 * Set of two die
 * @author Mitchel Charette
 */
public class Dice {
    
    private Die [] dice;

    public Dice(){
    this(2);
}
    
    public Dice(int numDie){
        dice = new Die[numDie];
        for(int i=0;i<numDie;++i){
            dice[i]= new Die();
        }
    }
    
    public Die getDie(int index){
        return dice[index];
        
    }
    
    public int getSize(){
        return dice.length;
    }
    
    public int roll(){
        int total = 0;
        for(Die die : dice){
            total += die.roll();
        }
        return total;
    }
    
    public int getValue(){
        int total = 0;
        for(Die die : dice){
            total += die.getValue();
        }
        return total;
    }
    
}
