      // @author Mitchel Charette
package dicegame;

import acm.graphics.GLabel;
import acm.program.GraphicsProgram;
import acm.util.RandomGenerator;
import java.awt.Color;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import svu.csc213.Dialog;

public class DiceGame extends GraphicsProgram {

    //can do private Dice dice = new Dice(); instead of the public DiceGame()
    private JButton wagerButton;
    private JButton rollButton;
    private JButton rerollButton;
    private JButton exitButton;
    private GLabel wagerLabel;
    private GLabel balanceLabel;
    private int balance = 100;
    private int wager = 10;
    private GDice dice = new GDice(new Dice());
    private int point = 0;
    private GLabel showPoint;
    private JButton resetButton;
    
    @Override
    public void init() {
        setBackground(Color.green);
        resetButton = new JButton("Reset");
        add(resetButton, SOUTH);
        wagerButton = new JButton("Wager");
        add(wagerButton, SOUTH);
        rollButton = new JButton("Roll");
        add(rollButton, SOUTH);
        rerollButton = new JButton("Reroll");
        add(rerollButton, SOUTH);
        rerollButton.setEnabled(false);
        exitButton = new JButton("Exit");
        add(exitButton, SOUTH);
        wagerLabel = new GLabel("Wager: $" + wager);
        wagerLabel.setFont("times-20");
        add(wagerLabel, 20, getHeight() - wagerLabel.getHeight() * 1.5 - 10);
        balanceLabel = new GLabel("Balance: $" + balance);
        balanceLabel.setFont("times-20");
        add(balanceLabel, getWidth() - 20 - balanceLabel.getWidth(), getHeight() - balanceLabel.getHeight() * 1.5 - 10);
        showPoint = new GLabel("");
        showPoint.setFont("times-20");
        add(showPoint, getWidth()/2 - 50, getHeight() - 280);
        addActionListeners();
        add(dice, this.getWidth() / 2 - dice.getWidth() / 2, 100);

    }

    @Override
    public void run() {
        
    }

    public void reset(){
        wager = 10;
        wagerLabel.setLabel("Wager: $" + wager);
        balance = 100;
        balanceLabel.setLabel("Balance: $" + balance);
        showPoint.setLabel("");
        rerollButton.setEnabled(false);
        wagerButton.setEnabled(true);
        rollButton.setEnabled(true);
        this.update(this.getGraphics());  
    }
    
    public void wager() {
        boolean goodWager = false;
        while (!goodWager) {
            int newWager = Dialog.getInteger(this, "Enter new wager?");
            if (newWager < 1 || newWager > balance) {
                Dialog.showMessage(this, "Please enter a wager between 1 and" + balance);
            } else {
                wager = newWager;
                wagerLabel.setLabel("Wager: $" + wager);
                goodWager = true;
            }
        }
        
    }

    public void roll() {
        rerollButton.setEnabled(true);
        wagerButton.setEnabled(false);
        rollButton.setEnabled(false);
       for(int i=0;i<RandomGenerator.getInstance().nextInt(10,50);i++){
           int total = dice.roll();
           this.update(this.getGraphics()); 
           pause(100);
       }
        int total = dice.roll();
        this.update(this.getGraphics());
        pause(500);
        if (total == 7 || total == 11) {
            Dialog.showMessage(this, "That's a natural.");
            balance += wager;
            balanceLabel.setLabel("Balance: $" + balance);
            restart();
        } else if (total == 2 || total == 3 || total == 12) {
            Dialog.showMessage(this, "That's craps. You lose. Keep trying though!");
            balance -= wager;
            balanceLabel.setLabel("Balance: $" + balance);
            restart();
        } else {
            point = total;
            Dialog.showMessage(this, "Your point is " + point + ".");
            showPoint.setLabel("Your point: " + point);
        }
//Dialog.showMessage(this,"You rolled the dice.");
        this.update(this.getGraphics());
        if (balance<1){
            Dialog.showMessage("Looks like you are out of cash. Press 'Reset' to reset your balance to $100 or 'exit' to exit.");
            rerollButton.setEnabled(false);
            wagerButton.setEnabled(false);
            rollButton.setEnabled(false);
            this.update(this.getGraphics());
        }
    }

    public void restart() {
        showPoint.setLabel("");
        rerollButton.setEnabled(false);
        wagerButton.setEnabled(true);
        rollButton.setEnabled(true);
        this.update(this.getGraphics());
    }

    public void reroll() {
        for(int i=0;i<RandomGenerator.getInstance().nextInt(10,30);i++){
           int total = dice.roll();
           this.update(this.getGraphics()); 
           pause(100);
       }
        int total = dice.roll();
           this.update(this.getGraphics()); 
        if (total == point) {
            Dialog.showMessage("You made your point.");
            balance += wager;
            balanceLabel.setLabel("Balance: $" + balance);
            restart();
        } else if (total == 7) {
            Dialog.showMessage("Too bad, you lose ¯\\_(ツ)_/¯");
            balance -= wager;
            balanceLabel.setLabel("Balance: $" + balance);
            restart();
        }
        if (balance<1){
            Dialog.showMessage("Looks like you are out of cash. Press 'reset' to reset your balance to $100 or 'exit' to exit.");
            rerollButton.setEnabled(false);
            wagerButton.setEnabled(false);
            rollButton.setEnabled(false);
            this.update(this.getGraphics());
        }
    }

    @Override
    public void exit() {
        System.exit(0);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        switch (ae.getActionCommand()) {
            case "Wager":
                wager();
                break;
            case "Roll":
                roll();
                break;
            case "Reroll":
                reroll();
                break;
            case "Exit":
                exit();
                break;
            case "Reset":
                reset();
                break;
            default:
                //Dialog.showMessage(this,"Something else.");

        }

    }

    public static void main(String[] args) {
        new DiceGame().start();
    }

}
