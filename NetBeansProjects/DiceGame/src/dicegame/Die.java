
package dicegame;

import acm.util.RandomGenerator;

/**
 *
 * @author Mitchel Charette
 */
//represents a single die, the number of sides is represented in the roll() as the nextInt
public class Die {
    
    private int value=1;
    
    // rolls the die
    public int roll(){
        value = RandomGenerator.getInstance().nextInt(1,6);
        return value;
    }
    
    //shows the current value on the die
    public int getValue(){
        return value;
    }
}
