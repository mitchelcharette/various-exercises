/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dicegame;

import acm.graphics.GCompound;

/**
 *
 * @author student
 */
public class GDice extends GCompound {
    
    private GDie[] gdice;
    
    public GDice(Dice dice){
        gdice = new GDie[dice.getSize()];
        for(int i =0;i<gdice.length;i++){
            gdice[i] = new GDie(dice.getDie(i));
            add(gdice[i],(gdice[i].getWidth()+20)*i,0);
        }
    }
    
    public int roll(){
        int total = 0;
        for(GDie gdie : gdice){
            total += gdie.roll();
    }
    return total;}
    
    public int getTotal(){
        int total = 0;
        for (GDie gdie : gdice){
            total += gdie.roll();
        }
        return total;
    }
    
}
