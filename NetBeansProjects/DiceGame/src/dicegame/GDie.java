/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dicegame;

import acm.graphics.GCompound;
import acm.graphics.GOval;
import acm.graphics.GRect;
import java.awt.Color;

/**Creates a graphical representation of a die
 *
 * @author Mitchel Charette
 */
public class GDie extends GCompound {
    private Die die;
    private GOval pip11;
    private GOval pip13;
    private GOval pip21;
    private GOval pip22;
    private GOval pip23;
    private GOval pip31;
    private GOval pip33;
    
    public GDie(Die die){
        this.die = die;
        // set up graphical display
        GRect face = new GRect(70,70);
        face.setFillColor(Color.white);
        face.setFilled(true);
        add(face);
        pip11 = new GOval(10,10);
        pip11.setFilled(true);
        add(pip11,10,10);
        pip13 = new GOval(10,10);
        pip13.setFilled(true);
        add(pip13,50,10);
        pip21 = new GOval(10,10);
        pip21.setFilled(true);
        add(pip21,10,30);
        pip22 = new GOval(10,10);
        pip22.setFilled(true);
        add(pip22,30,30);
        pip23 = new GOval(10,10);
        pip23.setFilled(true);
        add(pip23,50,30);
        pip31 = new GOval(10,10);
        pip31.setFilled(true);
        add(pip31,10,50);
        pip33 = new GOval(10,10);
        pip33.setFilled(true);
        add(pip33,50,50);
        synchDisplay();
    }
    
    public int roll(){
        die.roll();
        synchDisplay();
        //update displayed pips
        return die.getValue();
        
        
    }

    public int getValue(){
        return die.getValue();
    }
    
    private void synchDisplay(){
        pip11.setVisible(false);
        pip13.setVisible(false);
        pip21.setVisible(false);
        pip22.setVisible(false);
        pip23.setVisible(false);
        pip31.setVisible(false);
        pip33.setVisible(false);
        
        
        switch(die.getValue()){
            case 1:
                pip22.setVisible(true);
                break;
            case 2:
                pip11.setVisible(true);
                pip33.setVisible(true);
                break;
            case 3:
                pip11.setVisible(true);
                pip22.setVisible(true);
                pip33.setVisible(true);
                break;
            case 4:
                pip11.setVisible(true);
                pip13.setVisible(true);
                pip31.setVisible(true);
                pip33.setVisible(true);
                break;
            case 5:
                pip11.setVisible(true);
                pip13.setVisible(true);
                pip22.setVisible(true);
                pip31.setVisible(true);
                pip33.setVisible(true);
                break;
            case 6:
                pip11.setVisible(true);
                pip13.setVisible(true);
                pip21.setVisible(true);
                pip23.setVisible(true);
                pip31.setVisible(true);
                pip33.setVisible(true);
                break;
        }
    }
    
}
