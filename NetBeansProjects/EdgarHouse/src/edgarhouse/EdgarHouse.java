/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edgarhouse;

import acm.graphics.GOval;
import acm.graphics.GPolygon;
import acm.graphics.GRect;
import acm.program.GraphicsProgram;
import java.awt.Color;

/**
 *
 * @author student
 */
public class EdgarHouse extends GraphicsProgram{

   @Override
   public void run(){
       this.setBackground(new Color(45, 50, 50));
       GRect grass = new GRect(getWidth(),getHeight()/2);
       grass.setColor(new Color(40, 110,60));
       grass.setFillColor(new Color(40, 159, 60));
       grass.setFilled(true);
       add(grass,0,getHeight()*3.0/4.0);
       
       drawEdgarHouse(125,50);
       drawTrees(600, 250, 15);
       
   }
    
    
    public static void main(String[] args) {
        new EdgarHouse().start();
    }

    private void drawEdgarHouse(int xpos, int ypos) {
        //draw left spire
        drawCappedRect(75,400,xpos,ypos,Color.gray,Color.black);
        //draw house
        drawCappedRect(225,300,xpos+75,ypos+100,Color.gray,Color.black);
        //draw right spire
        drawCappedRect(75,400,xpos+300,ypos,Color.gray,Color.black);
        //draw entryway
        drawCappedRect(50,100,xpos+75+112-25,ypos+300,Color.black,Color.black);
        //draw left window
        GOval leftWindow = new GOval(50,50);
        //draw right window
        GOval rightWindow = new GOval(50,50);
        leftWindow.setFilled(true);
        rightWindow.setFilled(true);
        add(leftWindow,xpos+100,ypos+225);
        add(rightWindow,xpos+225,ypos+225);
        
    }

    private void drawTrees(int xpos, int ypos, int spacing) {
       for(int i=0; i<3;++i){
           drawCappedRect(20,200,xpos+i*(20+spacing),ypos,new Color(120, 51, 0),new Color(20, 150, 80));
       }
    }
    private void drawCappedRect(int width, int height, int xpos, int ypos, Color bodyColor, Color capColor){
       GPolygon cap = new GPolygon();
       cap.addVertex(xpos+width/2.0, ypos);
       cap.addVertex(xpos, ypos+height/5.0);
       cap.addVertex(xpos+width, ypos+height/5.0); 
       add(cap);
       GRect rect = new GRect(xpos,ypos+height/5.0,width,height*4.0/5.0);
       add(rect);
       rect.setFilled(true);
       rect.setFillColor(bodyColor);
       cap.setFilled(true);
       cap.setFillColor(capColor);
    }
}
