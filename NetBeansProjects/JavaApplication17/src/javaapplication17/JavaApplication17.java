/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication17;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class JavaApplication17 {

  
    public void Test_connection(String db_path, String username, String password)
    {
        try
        {
            Class.forName("com.mysql.cj.jdbc.Driver"); // new Driver
            Connection db_conx = null;
            db_conx = DriverManager.getConnection(db_path, username, password);
            
            //User Feedback
            if(db_conx != null)
                System.out.println("Database successfully connected !");
        }
        catch (SQLException es) //system handeling errors
        {
            System.err.format("SQL State: ", es.getSQLState());
            System.err.format("\n SQLException message: " + es.getMessage());
            System.err.format("\n VendorError: "+es.getErrorCode());
        }
        catch(Exception est)
                {
                 est.printStackTrace();  
                }
      
    }
    
    public static void main(String[] args)
    {
        DBMS_connection dc = new DBMS_connection();
        
        String db_path = "jdbc:mysql://localhost/university";
        String username = "root";
        String password = "";
        
        dc.Test_connection(db_path, username, password);
    }
    
}
