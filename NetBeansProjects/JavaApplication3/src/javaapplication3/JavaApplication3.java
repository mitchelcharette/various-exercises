package javaapplication3;

import acm.graphics.GOval;
import acm.graphics.GRect;
import acm.program.GraphicsProgram;
import java.awt.Color;
import svu.csc213.Dialog;

public class JavaApplication3 extends GraphicsProgram {

    float BRICK_HEIGHT = 20;
    float BRICK_WIDTH = 60;
    float centerX;

    @Override
    public void run() {
        float numBase = Dialog.getInteger(this, "How many bricks do you want in the base?");
        if (numBase>10) {
             BRICK_HEIGHT = 15;
             BRICK_WIDTH = 45;
        }
        centerX = this.getWidth() / 2;
      this.setBackground(new Color(100, 150, 250));
        GOval sun = new GOval(100,100);
        sun.setColor(Color.yellow);
        sun.setFillColor(Color.yellow);
        sun.setFilled(true);
        add(sun,25,25);
        GRect sand = new GRect (this.getWidth(),this.getHeight()/3);
        sand.setFillColor(new Color(194, 194, 128));
        sand.setColor(new Color(194, 194, 128));
        sand.setFilled(true);
        add(sand,0,this.getHeight()*2.0/3.0);

        for (int row = 1; row <= numBase; row++) {
            for (int col = 1; col <= row; col++) {
                GRect brick = new GRect(BRICK_WIDTH, BRICK_HEIGHT);
                brick.setFillColor(Color.black);
                brick.setFilled(true);
                add(brick, centerX -row*(BRICK_WIDTH/2) + col*(BRICK_WIDTH), (this.getHeight()-150 - numBase*BRICK_HEIGHT) + row * BRICK_HEIGHT);
            }
        }

    }

    public static void main(String[] args) {
        new JavaApplication3().start();
    }

}
