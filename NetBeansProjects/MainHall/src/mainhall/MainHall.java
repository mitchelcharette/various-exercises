/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mainhall;

import acm.graphics.GLine;
import acm.graphics.GOval;
import acm.graphics.GPolygon;
import acm.graphics.GRect;
import acm.program.GraphicsProgram;
import java.awt.Color;

/**
 *
 * @author student
 */
public class MainHall extends GraphicsProgram {

    @Override
    public void run() {
        this.setBackground(new Color(47, 141, 255));
        GRect grass = new GRect(this.getWidth(), 200);
        grass.setFilled(true);
        grass.setFillColor(new Color(53, 76, 25));
        add(grass, 0, this.getHeight() - 200);
        GOval cloud = new GOval(90, 50);
        cloud.setColor(Color.white);
        cloud.setFillColor(Color.white);
        cloud.setFilled(true);
        add(cloud, 75, 75);
        GOval cloud1 = new GOval(90, 50);
        cloud1.setColor(Color.white);
        cloud1.setFillColor(Color.white);
        cloud1.setFilled(true);
        add(cloud1, 55, 55);
        GOval cloud2 = new GOval(90, 50);
        cloud2.setColor(Color.white);
        cloud2.setFillColor(Color.white);
        cloud2.setFilled(true);
        add(cloud2, 125, 65);
        GOval cloud3 = new GOval(90, 50);
        cloud3.setColor(Color.white);
        cloud3.setFillColor(Color.white);
        cloud3.setFilled(true);
        add(cloud3, 650, 39);
        GOval cloud4 = new GOval(90, 50);
        cloud4.setColor(Color.white);
        cloud4.setFillColor(Color.white);
        cloud4.setFilled(true);
        add(cloud4, 625, 10);
        GOval cloud5 = new GOval(90, 50);
        cloud5.setColor(Color.white);
        cloud5.setFillColor(Color.white);
        cloud5.setFilled(true);
        add(cloud5, 590, 33);
        GRect building = new GRect(650, 200);
        building.setColor(new Color(131, 3, 0));
        building.setFillColor(new Color(131, 3, 0));
        building.setFilled(true);
        add(building, this.getWidth() / 2 - building.getWidth() / 2, 200);
        GPolygon cap = new GPolygon();
        cap.addVertex(building.getX(), building.getY());
        cap.addVertex(building.getX() + 30, building.getY() - 40);
        cap.addVertex(building.getX() + building.getWidth() - 30, building.getY() - 40);
        cap.addVertex(building.getX() + building.getWidth(), building.getY());
        add(cap);
        cap.setFilled(true);
        cap.setFillColor(new Color(102, 102, 102));
        drawTower(200, 110, 100, 290, true, 200);
        drawTower(300, 200, 150, 200, false, 60);
        drawTower(450, 155, 100, 245, true, 200);
        //leftbuilding
        drawWindow(60, 220, 20, 40, 9, 5);
        drawWindow(60, 300, 20, 40, 9, 5);
        //right building
        drawWindow(560, 220, 20, 40, 9, 5);
        drawWindow(560, 300, 20, 40, 9, 5);
        //1st tower
        drawWindow(205, 130, 10, 40, 9, 1);
        drawWindow(224, 130, 20, 40, 9, 2);
        drawWindow(282, 130, 10, 40, 9, 1);
        drawWindow(205, 220, 10, 40, 9, 1);
        drawWindow(224, 220, 20, 40, 9, 2);
        drawWindow(282, 220, 10, 40, 9, 1);
        drawWindow(205, 300, 10, 40, 9, 1);
        drawWindow(224, 300, 20, 40, 9, 2);
        drawWindow(282, 300, 10, 40, 9, 1);

        //2nd tower
        drawWindow(455, 160, 10, 40, 9, 1);
        drawWindow(474, 160, 20, 40, 9, 2);
        drawWindow(532, 160, 10, 40, 9, 1);
        drawWindow(455, 220, 10, 40, 9, 1);
        drawWindow(474, 220, 20, 40, 9, 2);
        drawWindow(532, 220, 10, 40, 9, 1);
        drawWindow(455, 300, 10, 40, 9, 1);
        drawWindow(474, 300, 20, 40, 9, 2);
        drawWindow(532, 300, 10, 40, 9, 1);

//pilars
        drawPilar(52, 275, 5, 80, 7, 41);
        drawPilar(52, 201, 5, 84, 7, 41);
        drawPilar(452, 275, 5, 80, 7, 41);
        drawPilar(452, 201, 5, 84, 7, 41);

//railings
        drawRailing(52, 355, 40, 250, 20, 3);
        drawRailing(52, 275, 40, 250, 20, 3);
        drawRailing(452, 355, 40, 250, 20, 3);
        drawRailing(452, 275, 40, 250, 20, 3);

        for (int i = 0; i < 5; i++) {
            GRect stair = new GRect(148 + i * 40, 15);
            stair.setFilled(true);
            stair.setFillColor(new Color(139, 64, 0));
            add(stair, building.getX() + building.getWidth() / 2 - (75 + i * 20) + 1, 350 + i * 15);
        }

        GPolygon doorPlace = new GPolygon();
        doorPlace.addVertex(350, 350);
        doorPlace.addVertex(425, 350);
        doorPlace.addArc(95, 150, 0, 180);
        add(doorPlace);
        doorPlace.setFillColor(new Color(230, 230, 230));
        doorPlace.setFilled(true);
        GRect door = new GRect(55, 60);
        add(door, 350, 290);
        GRect doorWindow = new GRect(55, 20);
        doorWindow.setFilled(true);
        doorWindow.setFillColor(new Color(117, 218, 255));
        add(doorWindow, 350, 300);
        GLine doorMid = new GLine(377, 290, 377, 350);
        add(doorMid);

        GPolygon windowPlace = new GPolygon();
        windowPlace.addVertex(425, 260);
        windowPlace.addArc(95, 100, 0, 180);
        windowPlace.setFillColor(new Color(117, 218, 255));
        windowPlace.setFilled(true);
        add(windowPlace);
        GRect bar = new GRect(5, 50);
        bar.setFilled(true);
        bar.setFillColor(Color.white);
        add(bar, 375, 210);

        drawRailing(304, 223, 20, 151, 20, 3);
        

    }

    public static void main(String[] args) {
        new MainHall().start();
    }

    private void drawWindow(int xpos, int ypos, int width, int height, int spacing, int numberOfWindows) {
        for (int i = 0; i < numberOfWindows; i++) {
            GRect window = new GRect(width, height);
            window.setFillColor(new Color(117, 218, 255));
            window.setFilled(true);
            add(window, xpos + i * (width + spacing), ypos);
            GRect windowBar = new GRect(width, height / 10.0);
            windowBar.setFillColor(Color.white);
            windowBar.setFilled(true);
            add(windowBar, xpos + i * (spacing + width), ypos + height / 2 - height / 20.0);
        }
    }

    private void drawPilar(int xpos, int ypos, int width, int height, int numberPilars, int spacing) {
        for (int i = 0; i < numberPilars; i++) {
            GRect pilar = new GRect(width, height);
            pilar.setFillColor(Color.white);
            pilar.setFilled(true);
            pilar.setColor(Color.white);
            add(pilar, xpos + i * spacing, ypos);
        }
        GRect top = new GRect(numberPilars * spacing - spacing, 12);
        top.setFillColor(Color.white);
        top.setColor(Color.white);
        top.setFilled(true);
        add(top, xpos, ypos);
    }

    private void drawRailing(int xpos, int ypos, int numberRails, int length, int height, int railWidth) {
        for (int i = 0; i < numberRails; i++) {
            GRect rail = new GRect(railWidth, height);
            rail.setColor(Color.white);
            rail.setFillColor(Color.white);
            rail.setFilled(true);
            add(rail, xpos + i * length / numberRails, ypos - height);
        }

        GRect bottom = new GRect(length - length / numberRails + railWidth, height / 8.0);
        bottom.setColor(Color.white);
        bottom.setFillColor(Color.white);
        bottom.setFilled(true);
        add(bottom, xpos, ypos);
        GRect top = new GRect(length - length / numberRails + railWidth, height / 8.0);
        top.setFillColor(Color.white);
        top.setFilled(true);
        top.setColor(Color.white);
        top.setColor(Color.white);
        add(top, xpos, ypos - height - height / 8.0);

    }

    private void drawTower(int xpos, int ypos, int width, int height, boolean yes, int capHeight) {
        GRect tower = new GRect(width, height);
        tower.setFillColor(new Color(131, 3, 0));
        tower.setFilled(true);
        add(tower, xpos, ypos);
        GPolygon cap = new GPolygon();
        cap.addVertex(xpos, ypos);
        cap.addVertex(xpos + width, ypos);
        cap.setFillColor(Color.black);
        cap.setFilled(true);
        if (yes) {
            cap.addArc(width, capHeight, 0, 180);
            add(cap);
        } else {
            cap.addVertex(xpos + width / 2, ypos - capHeight);
        }
        add(cap);
    }

    
}
