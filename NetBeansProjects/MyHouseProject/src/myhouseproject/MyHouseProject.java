/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myhouseproject;

import acm.graphics.GLine;
import acm.graphics.GOval;
import acm.graphics.GPolygon;
import acm.graphics.GRect;
import acm.program.GraphicsProgram;
import java.awt.Color;

/**
 *
 * @author student
 */
public class MyHouseProject extends GraphicsProgram{
    
    @Override
    public void run(){
        //draw the house
        this.setBackground(Color.blue);
        GRect house = new GRect (500,250);
        GRect grass = new GRect (this.getWidth(),this.getHeight());
        grass.setColor(Color.green);
        grass.setFillColor(Color.green);
        grass.setFilled(true);
        add(grass,0,this.getHeight()/1.5);
        house.setColor(Color.LIGHT_GRAY);
        house.setFillColor(Color.LIGHT_GRAY);
        house.setFilled(true);
        add(house,this.getWidth()/4,this.getHeight()-350);
        GRect fence  = new GRect (25,75);
        GRect fence2  = new GRect (25,75);
        GRect fence3  = new GRect (25,75);
        GRect fence4  = new GRect (25,75);
        GRect fence5  = new GRect (25,75);
        GRect fence6  = new GRect (25,75);
        GRect fence7  = new GRect (13,75);
        GRect fence8  = new GRect (25,75);
        GRect fence9 = new GRect (25,75);
        GRect fence1 = new GRect (25,75);
        fence.setColor(Color.black);
        fence.setFillColor(Color.yellow);
        fence.setFilled(true);
        add(fence,25,this.getHeight()/1.5-75);
        fence2.setColor(Color.black);
        fence2.setFillColor(Color.yellow);
        fence2.setFilled(true);
        add(fence2,50,this.getHeight()/1.5-75);
        fence3.setColor(Color.black);
        fence3.setFillColor(Color.yellow);
        fence3.setFilled(true);
        add(fence3,75,this.getHeight()/1.5-75);
        fence4.setColor(Color.black);
        fence4.setFillColor(Color.yellow);
        fence4.setFilled(true);
        add(fence4,100,this.getHeight()/1.5-75);
        fence5.setColor(Color.black);
        fence5.setFillColor(Color.yellow);
        fence5.setFilled(true);
        add(fence5,125,this.getHeight()/1.5-75);
        fence6.setColor(Color.black);
        fence6.setFillColor(Color.yellow);
        fence6.setFilled(true);
        add(fence6,150,this.getHeight()/1.5-75);
        fence7.setColor(Color.black);
        fence7.setFillColor(Color.yellow);
        fence7.setFilled(true);
        add(fence7,175,this.getHeight()/1.5-75);
        fence8.setColor(Color.black);
        fence8.setFillColor(Color.yellow);
        fence8.setFilled(true);
        add(fence8,713,this.getHeight()/1.5-75);
        fence9.setColor(Color.black);
        fence9.setFillColor(Color.yellow);
        fence9.setFilled(true);
        add(fence9,688,this.getHeight()/1.5-75);
        fence1.setColor(Color.black);
        fence1.setFillColor(Color.yellow);
        fence1.setFilled(true);
        add(fence1,738,this.getHeight()/1.5-75);
        GRect panel = new GRect(500,50);
        GRect panel2 = new GRect(500,50);
        GRect panel3 = new GRect(500,50);
        GRect panel4 = new GRect(500,50);
        GRect panel5 = new GRect(500,50);
        add(panel,this.getWidth()/4,this.getHeight()-350);
        add(panel2,this.getWidth()/4,this.getHeight()-300);
        add(panel3,this.getWidth()/4,this.getHeight()-250);
        add(panel4,this.getWidth()/4,this.getHeight()-200);
       add(panel5,this.getWidth()/4,this.getHeight()-150);
        GRect pillar = new GRect (25,250);
       pillar.setColor(Color.black);
       pillar.setFillColor(Color.lightGray);
       pillar.setFilled(true);
       add(pillar,0,this.getHeight()-350);
       GRect carport = new GRect (this.getWidth()/4,35);
       carport.setColor(Color.black);
       carport.setFillColor(Color.LIGHT_GRAY);
       carport.setFilled(true);
       add(carport,0,this.getHeight()-350);
       GRect drive = new GRect(100,300);
       drive.setColor(Color.black);
       drive.setFillColor(Color.black);
       drive.setFilled(true);
       add(drive,60,this.getHeight()/1.5);
       GRect door = new GRect (100,150);
       door.setColor(Color.black);
       door.setFillColor(Color.red);
       door.setFilled(true);
       add(door,this.getWidth()/2+20,this.getHeight()/2-4);
       GOval knob = new GOval (15,10);
       knob.setColor(Color.black);
       knob.setFillColor(Color.BLACK);
       knob.setFilled(true);
       add(knob,470,310);
       GRect window1 = new GRect (40,100);
       window1.setColor(Color.black);
       window1.setFillColor(Color.white);
       window1.setFilled(true);
       add(window1,215,217);
       GRect window2 = new GRect (40,100);
       window2.setColor(Color.black);
       window2.setFillColor(Color.white);
       window2.setFilled(true);
       add(window2,255,217);
       GRect window3 = new GRect (40,100);
       window3.setColor(Color.black);
       window3.setFillColor(Color.white);
       window3.setFilled(true);
       add(window3,295,217);
       GRect window = new GRect (100,60);
       window.setColor(Color.black);
       window.setFillColor(Color.white);
       window.setFilled(true);
       add(window,550,200);
       GOval cloud = new GOval (90,50);
       cloud.setColor(Color.white);
       cloud.setFillColor(Color.white);
       cloud.setFilled(true);
       add(cloud,75,75);
       GOval cloud1 = new GOval (90,50);
       cloud1.setColor(Color.white);
       cloud1.setFillColor(Color.white);
       cloud1.setFilled(true);
       add(cloud1,55,55);
       GOval cloud2 = new GOval (90,50);
       cloud2.setColor(Color.white);
       cloud2.setFillColor(Color.white);
       cloud2.setFilled(true);
       add(cloud2,125,65);
       GOval cloud3 = new GOval (90,50);
       cloud3.setColor(Color.white);
       cloud3.setFillColor(Color.white);
       cloud3.setFilled(true);
       add(cloud3,650,39);
       GOval cloud4 = new GOval (90,50);
       cloud4.setColor(Color.white);
       cloud4.setFillColor(Color.white);
       cloud4.setFilled(true);
       add(cloud4,625,10);
       GOval cloud5 = new GOval (90,50);
       cloud5.setColor(Color.white);
       cloud5.setFillColor(Color.white);
       cloud5.setFilled(true);
       add(cloud5,590,33);
       GPolygon roof = new GPolygon();
       roof.addVertex(this.getWidth()/4,this.getHeight()-350);
       roof.addVertex(this.getWidth()/4+500,this.getHeight()-350);
       roof.addVertex(this.getWidth()/4+250,25);
       roof.setColor(Color.black);
       roof.setFillColor(Color.black);
       roof.setFilled(true);
       add(roof);
       GRect tree = new GRect(55,220);
       tree.setColor(new Color(139,69,19));
       tree.setFillColor(new Color(139,69,19));
       tree.setFilled(true);
       add(tree,640,230);
       GOval leaf = new GOval(160,130);
       leaf.setColor(new Color (0,168,107));
       leaf.setFillColor(new Color (0,168,107));
       leaf.setFilled(true);
       add(leaf,590,150);
       
       
       
     
        
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       new MyHouseProject().start(); // TODO code application logic here
    }
    
}
