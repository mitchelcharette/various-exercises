/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pyramid;

import acm.graphics.GOval;
import acm.graphics.GRect;
import acm.program.GraphicsProgram;
import java.awt.Color;

/**
 *
 * @author student
 */
public class Pyramid extends GraphicsProgram {

    @Override
    public void run() {
            for (int row = 0; row < 8; ++row) {
                for (int col = 0; col < 8; ++col) {
                    GRect square = new GRect(40, 40);
                    if ((row + col) % 2 == 0) {
                        square.setFillColor(Color.white);
                    } else {
                        square.setFillColor(Color.black);
                    }
                    square.setFilled(true);
                    add(square, 50 + col * square.getWidth(), 50 + row * square.getHeight());
                    if((row<3 || row>4) && square.getFillColor()==Color.white){
                      GOval checker = new GOval(30,30);
                      checker.setFilled(true);
                      if(row<3){
                      checker.setFillColor(Color.red);
                      add(checker,square.getX()+5,square.getY()+5);
                      }else{
                          checker.setFillColor(Color.black);
                          add(checker,square.getX()+5,square.getY()+5);
                      }
                    }

                }
            }
        }

    public static void main(String[] args) {
        new Pyramid().start();
    }

}
