/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thepyramid;

import acm.graphics.GOval;
import acm.graphics.GRect;
import acm.program.GraphicsProgram;
import acm.util.RandomGenerator;
import java.awt.Color;
import svu.csc213.Dialog;



public class ThePyramid extends GraphicsProgram {

    float BRICK_HEIGHT = 20;
    float BRICK_WIDTH = 60;
    float centerX;
 
    @Override
    public void run() {
         float numBase = Dialog.getInteger(this, "How many bricks do you want in the base?");
        if (numBase>10) {
             BRICK_HEIGHT = 15;
             BRICK_WIDTH = 45;
        }
        centerX = this.getWidth() / 2;
        this.setBackground(new Color(100, 150, 250));
        GOval sun = new GOval(100,100);
        sun.setColor(Color.yellow);
        sun.setFillColor(Color.yellow);
        sun.setFilled(true);
        add(sun,25,25);
        GRect sand = new GRect (this.getWidth(),this.getHeight()/3);
        sand.setFillColor(new Color(194, 194, 128));
        sand.setColor(new Color(194, 194, 128));
        sand.setFilled(true);
        add(sand,0,this.getHeight()*2.0/3.0);
            
       for (int row = 0; row <= numBase; row++) {
            for (int col = 0; col < row; col++) {
                GRect brick = new GRect(BRICK_WIDTH, BRICK_HEIGHT);
                brick.setFillColor(new Color(194,162,128));
                brick.setFilled(true);
                add(brick, centerX -row*(BRICK_WIDTH/2) + col*(BRICK_WIDTH), (this.getHeight()-150 - numBase*BRICK_HEIGHT) + row * BRICK_HEIGHT);
            }
        }
            double r = 100;
            double g = 150;
            double b = 250;
            RandomGenerator gen = RandomGenerator.getInstance();
            while(sun.getY()<this.getHeight() && r>0 && g>0){
                sun.move(4,2);
                r = r -.3;
                g = g -.4;
                b = Math.max(0.0,b -.4);
                this.setBackground(new Color((int)r,(int)g,(int)b));
                if (sun.getY()>this.getHeight()/2){
                int numStars = gen.nextInt(1,5);
                for(int s=0;s < numStars; ++s) {
                    GOval star = new GOval(gen.nextInt(1,4),gen.nextInt(1,4));
                    star.setFillColor(Color.white);
                    star.setFilled(true);
                    add(star,gen.nextInt(0,this.getWidth()),gen.nextInt(0,(int)sand.getY()));
                    star.sendToBack();
                }
                }
                pause(50);
    }
  
    }

    public static void main(String[] args) {
        new ThePyramid().start();
    }

}
