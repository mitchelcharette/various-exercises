/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thermometer;

import acm.graphics.GLabel;
import acm.graphics.GOval;
import acm.graphics.GRect;
import acm.program.GraphicsProgram;
import java.awt.Color;
import svu.csc213.Dialog;

/**
 *
 * @author student
 */
public class Thermometer extends GraphicsProgram {

    private final int THERMOMETER_WIDTH = 25;
    private final int THERMOMETER_HEIGHT = 300;
  
  
    @Override
    public void run(){
        // prompt for a value in farenheit
        // validate input
        // calculate in Celsius
        // create the graphics and label
        
        
        int farenheit = Dialog.getInteger(this,"Enter the number degrees in farenheit:");
        
        
        while(farenheit>212||farenheit<32) {
            Dialog.showMessage(this,"Please enter a value between 32 and 212");
            farenheit = Dialog.getInteger(this,"Enter the number degrees in farenheit:");
            
        }
            int celsius = (int) Math.round((farenheit-32.0)*5.0/9.0);
            GLabel farenheitLabel = new GLabel("Celsius:" + celsius);
            farenheitLabel.setFont("Times-32");
            GRect thermometer = new GRect(THERMOMETER_WIDTH, THERMOMETER_HEIGHT);
            thermometer.setFillColor(Color.red);
            thermometer.setFilled(true);
            add(thermometer, 
                    (getWidth()-thermometer.getWidth())/2,
                    (getHeight()-thermometer.getHeight())/2);
            GRect meterFill = new GRect(25,thermometer.getHeight()-(celsius*3));
            meterFill.setFillColor(Color.white);
            meterFill.setFilled(true);
            add(meterFill, thermometer.getX(), thermometer.getY());
            add(farenheitLabel,
                    (getWidth()-farenheitLabel.getWidth())/2+150,
                    thermometer.getY()+thermometer.getHeight()+farenheitLabel.getHeight()+20);
            GOval bottom = new GOval (40,40);
            bottom.setFillColor(Color.red);
            bottom.setFilled(true);
            add(bottom,getWidth()/2-bottom.getWidth()/2,getHeight()/2-bottom.getHeight()/2+thermometer.getHeight()/2+10);
        
        
        
    }
    
    
    
    public static void main(String[] args) {
        new Thermometer().start();
        // TODO code application logic here
    }
    
}
