/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thermometer;

import acm.graphics.GLabel;
import acm.graphics.GLine;
import acm.graphics.GOval;
import acm.graphics.GRect;
import acm.program.GraphicsProgram;
import java.awt.Color;
import svu.csc213.Dialog;

/**
 *
 * @author student
 */
public class Thermometer extends GraphicsProgram {

    private final int THERMOMETER_WIDTH = 25;
    private final int THERMOMETER_HEIGHT = 300;
  
  
    @Override
    public void run(){
        // prompt for a value in farenheit
        // validate input
        // calculate in Celsius
        // create the graphics and label
        
        
        int farenheit = Dialog.getInteger(this,"Enter the number degrees in farenheit:");
        
        
        while(farenheit>212||farenheit<32) {
            Dialog.showMessage(this,"Please enter a value between 32 and 212");
            farenheit = Dialog.getInteger(this,"Enter the number degrees in farenheit:");
            
        }
        int celsius = (int) Math.round((farenheit-32.0)*5.0/9.0);
        if (celsius<25){
            if (celsius<5){
                this.setBackground(Color.cyan);
                GRect snow = new GRect (this.getWidth(),this.getHeight()/5);
                snow.setColor(Color.white);
                snow.setFillColor(Color.white);
                snow.setFilled(true);
                add(snow,0,this.getHeight()-this.getHeight()/5);
            GOval snowmanBot = new GOval (60,60);
            snowmanBot.setFillColor(Color.white);
            snowmanBot.setFilled(true);
            add(snowmanBot,60,getHeight()-this.getHeight()/5-60);
            GOval snowmanMid = new GOval (50,50);
            snowmanMid.setFillColor(Color.white);
            snowmanMid.setFilled(true);
            add(snowmanMid,65,getHeight()-this.getHeight()/5-110);
            GLabel farenheitLabel = new GLabel("Celsius:" + celsius);
            farenheitLabel.setFont("Times-32");
            GRect thermometer = new GRect(THERMOMETER_WIDTH, THERMOMETER_HEIGHT);
            thermometer.setFillColor(Color.red);
            thermometer.setFilled(true);
            add(thermometer, 
                    (getWidth()-thermometer.getWidth())/2,
                    (getHeight()-thermometer.getHeight())/2);
            GRect meterFill = new GRect(25,thermometer.getHeight()-thermometer.getHeight()/50*celsius);
            meterFill.setFillColor(Color.white);
            meterFill.setFilled(true);
            add(meterFill, thermometer.getX(), thermometer.getY());
            add(farenheitLabel,
                    (getWidth()-farenheitLabel.getWidth())/2+150,
                    thermometer.getY()+thermometer.getHeight()+farenheitLabel.getHeight()+20);
            GOval bottom = new GOval (40,40);
            bottom.setFillColor(Color.red);
            bottom.setFilled(true);
            add(bottom,getWidth()/2-bottom.getWidth()/2,getHeight()/2-bottom.getHeight()/2+thermometer.getHeight()/2+17);
            GLine line1 = new GLine (thermometer.getX()+THERMOMETER_WIDTH,thermometer.getY()+THERMOMETER_HEIGHT*4/5,thermometer.getX()+thermometer.getWidth()/2,thermometer.getY()+THERMOMETER_HEIGHT*4/5);
            add(line1);
            GLine line2 = new GLine (thermometer.getX()+THERMOMETER_WIDTH,thermometer.getY()+THERMOMETER_HEIGHT*3/5,thermometer.getX()+thermometer.getWidth()/2,thermometer.getY()+THERMOMETER_HEIGHT*3/5);
            add(line2);
            GLine line3 = new GLine (thermometer.getX()+THERMOMETER_WIDTH,thermometer.getY()+THERMOMETER_HEIGHT*2/5,thermometer.getX()+thermometer.getWidth()/2,thermometer.getY()+THERMOMETER_HEIGHT*2/5);
            add(line3);
            GLine line4 = new GLine (thermometer.getX()+THERMOMETER_WIDTH,thermometer.getY()+THERMOMETER_HEIGHT*1/5,thermometer.getX()+thermometer.getWidth()/2,thermometer.getY()+THERMOMETER_HEIGHT*1/5);
            add(line4);
            GLabel fifthLine = new GLabel ("10 C");
            fifthLine.setFont("Times-18");
            GLabel twoFifthLine = new GLabel ("20 C");
            twoFifthLine.setFont("Times-18");
            GLabel threeFifthLine = new GLabel ("30 C");
            threeFifthLine.setFont("Times-18");
            GLabel fourFifthLine = new GLabel ("40 C");
            fourFifthLine.setFont("Times-18");
            GLabel top = new GLabel ("50 C");
            top.setFont("Times-18");
            add(top,thermometer.getX()+THERMOMETER_WIDTH+4,thermometer.getY()+7);
            add(fifthLine,thermometer.getX()+THERMOMETER_WIDTH+4,thermometer.getY()+THERMOMETER_HEIGHT*4/5+7);
            add(twoFifthLine,thermometer.getX()+THERMOMETER_WIDTH+4,thermometer.getY()+THERMOMETER_HEIGHT*3/5+7);
            add(threeFifthLine,thermometer.getX()+THERMOMETER_WIDTH+4,thermometer.getY()+THERMOMETER_HEIGHT*2/5+7);
            add(fourFifthLine,thermometer.getX()+THERMOMETER_WIDTH+4,thermometer.getY()+THERMOMETER_HEIGHT*1/5+7);
            
            }
            else {
                GLabel farenheitLabel = new GLabel("Celsius:" + celsius);
            farenheitLabel.setFont("Times-32");
            this.setBackground(new Color(137,207,245));
            GRect grass = new GRect (this.getWidth(),this.getHeight()/4);
                grass.setColor(new Color(76,187,23));
                grass.setFillColor(new Color(76,187,23));
                grass.setFilled(true);
                add(grass,0,this.getHeight()-this.getHeight()/4);
            GRect thermometer = new GRect(THERMOMETER_WIDTH, THERMOMETER_HEIGHT);
            thermometer.setFillColor(Color.red);
            thermometer.setFilled(true);
            add(thermometer, 
                    (getWidth()-thermometer.getWidth())/2,
                    (getHeight()-thermometer.getHeight())/2);
            GRect meterFill = new GRect(25,thermometer.getHeight()-thermometer.getHeight()/50*celsius);
            meterFill.setFillColor(Color.white);
            meterFill.setFilled(true);
            add(meterFill, thermometer.getX(), thermometer.getY());
            add(farenheitLabel,
                    (getWidth()-farenheitLabel.getWidth())/2+150,
                    thermometer.getY()+thermometer.getHeight()+farenheitLabel.getHeight()+20);
            GOval bottom = new GOval (40,40);
            bottom.setFillColor(Color.red);
            bottom.setFilled(true);
            add(bottom,getWidth()/2-bottom.getWidth()/2,getHeight()/2-bottom.getHeight()/2+thermometer.getHeight()/2+17);
            GLine line1 = new GLine (thermometer.getX()+THERMOMETER_WIDTH,thermometer.getY()+THERMOMETER_HEIGHT*4/5,thermometer.getX()+thermometer.getWidth()/2,thermometer.getY()+THERMOMETER_HEIGHT*4/5);
            add(line1);
            GLine line2 = new GLine (thermometer.getX()+THERMOMETER_WIDTH,thermometer.getY()+THERMOMETER_HEIGHT*3/5,thermometer.getX()+thermometer.getWidth()/2,thermometer.getY()+THERMOMETER_HEIGHT*3/5);
            add(line2);
            GLine line3 = new GLine (thermometer.getX()+THERMOMETER_WIDTH,thermometer.getY()+THERMOMETER_HEIGHT*2/5,thermometer.getX()+thermometer.getWidth()/2,thermometer.getY()+THERMOMETER_HEIGHT*2/5);
            add(line3);
            GLine line4 = new GLine (thermometer.getX()+THERMOMETER_WIDTH,thermometer.getY()+THERMOMETER_HEIGHT*1/5,thermometer.getX()+thermometer.getWidth()/2,thermometer.getY()+THERMOMETER_HEIGHT*1/5);
            add(line4);
            GLabel fifthLine = new GLabel ("10 C");
            fifthLine.setFont("Times-18");
            GLabel twoFifthLine = new GLabel ("20 C");
            twoFifthLine.setFont("Times-18");
            GLabel threeFifthLine = new GLabel ("30 C");
            threeFifthLine.setFont("Times-18");
            GLabel fourFifthLine = new GLabel ("40 C");
            fourFifthLine.setFont("Times-18");
            GLabel top = new GLabel ("50 C");
            top.setFont("Times-18");
            add(top,thermometer.getX()+THERMOMETER_WIDTH+4,thermometer.getY()+7);
            add(fifthLine,thermometer.getX()+THERMOMETER_WIDTH+4,thermometer.getY()+THERMOMETER_HEIGHT*4/5+7);
            add(twoFifthLine,thermometer.getX()+THERMOMETER_WIDTH+4,thermometer.getY()+THERMOMETER_HEIGHT*3/5+7);
            add(threeFifthLine,thermometer.getX()+THERMOMETER_WIDTH+4,thermometer.getY()+THERMOMETER_HEIGHT*2/5+7);
            add(fourFifthLine,thermometer.getX()+THERMOMETER_WIDTH+4,thermometer.getY()+THERMOMETER_HEIGHT*1/5+7);
            GOval sun = new GOval (200,200);
            sun.setColor(Color.yellow);
            sun.setFillColor(Color.yellow);
            sun.setFilled(true);
            add(sun,this.getWidth()-150,-50);
            
            }
            
            
    }
        else {
            // Background
            this.setBackground(new Color(137,207,245));
            
            GRect water = new GRect (this.getWidth(),this.getHeight()/4-15);
                water.setColor(new Color(16,52,166));
                water.setFillColor(new Color(16,52,166));
                water.setFilled(true);
                add(water,0,this.getHeight()-this.getHeight()/2+15);
            GRect beach = new GRect (this.getWidth(),this.getHeight()/4);
                beach.setColor(new Color(239,221,111));
                beach.setFillColor(new Color(239,221,111));
                beach.setFilled(true);
                add(beach,0,this.getHeight()-this.getHeight()/4);
          
            GOval sun = new GOval (200,200);
                sun.setColor(Color.yellow);
                sun.setFillColor(Color.yellow);
                sun.setFilled(true);
                add(sun,this.getWidth()-150,-50);
            
                // Thermometer
            GRect thermometer = new GRect(THERMOMETER_WIDTH, THERMOMETER_HEIGHT);
                thermometer.setFillColor(Color.red);
                thermometer.setFilled(true);
                add(thermometer, 
                    (getWidth()-thermometer.getWidth())/2,
                    (getHeight()-thermometer.getHeight())/2);
            GLabel farenheitLabel = new GLabel("Celsius:" + celsius);
                farenheitLabel.setFont("Times-32");
                 add(farenheitLabel,
                    (getWidth()-farenheitLabel.getWidth())/2+150,
                    thermometer.getY()+thermometer.getHeight()+farenheitLabel.getHeight()+20);
            GRect meterFill = new GRect(25,thermometer.getHeight()-thermometer.getHeight()/100*celsius);
                meterFill.setFillColor(Color.white);
                meterFill.setFilled(true);
                add(meterFill, thermometer.getX(), thermometer.getY());
            GOval bottom = new GOval (40,40);
                bottom.setFillColor(Color.red);
                bottom.setFilled(true);
                add(bottom,getWidth()/2-bottom.getWidth()/2,getHeight()/2-bottom.getHeight()/2+thermometer.getHeight()/2+17);
           
            GLine line1= new GLine (thermometer.getX()+thermometer.getWidth(),thermometer.getY()+thermometer.getHeight()/4,thermometer.getX()+thermometer.getWidth()/2,thermometer.getY()+thermometer.getHeight()/4);
            add(line1);
            GLine line2= new GLine (thermometer.getX()+thermometer.getWidth(),thermometer.getY()+thermometer.getHeight()/2,thermometer.getX()+thermometer.getWidth()/2,thermometer.getY()+thermometer.getHeight()/2);
            add(line2);
            GLine line3= new GLine (thermometer.getX()+thermometer.getWidth(),thermometer.getY()+thermometer.getHeight()/4+thermometer.getHeight()/2,thermometer.getX()+thermometer.getWidth()/2,thermometer.getY()+thermometer.getHeight()/4+thermometer.getHeight()/2);
            add(line3);
            GLabel quarterLabel = new GLabel ("25 C");
            quarterLabel.setFont("Times-18");
            GLabel halfLabel = new GLabel ("50 C");
            halfLabel.setFont("Times-18");
            GLabel threeQuarterLabel = new GLabel ("75 C");
            threeQuarterLabel.setFont("Times-18");
            add(quarterLabel,thermometer.getX()+thermometer.getWidth()+4,thermometer.getY()+thermometer.getHeight()/4+thermometer.getHeight()/2+7);   
            add(halfLabel,thermometer.getX()+thermometer.getWidth()+4,thermometer.getY()+thermometer.getHeight()/2+7);
            add(threeQuarterLabel,thermometer.getX()+thermometer.getWidth()+4,thermometer.getY()+thermometer.getHeight()/4+7);
        }
        
    }
    
    
    
    public static void main(String[] args) {
        new Thermometer().start();
        // TODO code application logic here
    }
    
}
