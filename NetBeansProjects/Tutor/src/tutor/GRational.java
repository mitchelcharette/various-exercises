/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tutor;

import acm.graphics.GCompound;
import acm.graphics.GLabel;
import acm.graphics.GRect;
import java.awt.Color;


/**
 *Creates graphical representation of rational numbers
 * 
 * @author student
 */
public class GRational extends GCompound {

    public final static double HEIGHT = 300.0;
    public final static double WIDTH = 150.0;
    
    private Rational r;
    
    public GRational(Rational r){
        this.r = r;
        for(int rectangle=0;rectangle<r.getDenominator();++rectangle){
            GRect rect = new GRect(WIDTH, HEIGHT/r.getDenominator());
//            if(r.getNumerator()>r.getDenominator()){
//                int x = r.getNumerator()/r.getDenominator();
//                GLabel hat = new GLabel(""+x+"");
//                hat.setFont("Times-32");
//                add(hat,rect.getX()-40,HEIGHT/2+(hat.getHeight())/2);
//            }
            if(r.getNumerator()<0){
                if(rectangle< Math.abs(r.getNumerator())){
                    rect.setFillColor(Color.red);
                    rect.setFilled(true);
                }
                    
            }
            if(rectangle>=r.getDenominator()-r.getNumerator()){
                rect.setFilled(true);
                rect.setFillColor(Color.blue);
                
            }
            add(rect,0,rectangle*HEIGHT/r.getDenominator());
        }
        GLabel label = new GLabel(r.toString());
        label.setFont("Times-28");
        add(label,(WIDTH-label.getWidth())/2,HEIGHT+label.getHeight());
    }
    
}
