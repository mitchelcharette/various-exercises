/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tutor;

/**
 * Rational numbers are fractions that can be represented by integers in the
 * numerator and the denominator
 */
public class Rational {

    private int numerator;
    private int denominator;

    public Rational(int numerator, int denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
        reduce();
    }

    public int getNumerator() {
        return numerator;
    }

    public int getDenominator() {
        return denominator;
    }

    
    
    
    public Rational add(Rational other) {
        return new Rational(numerator * other.denominator + other.numerator * denominator,
                denominator * other.denominator);
    }

    public Rational sub(Rational other) {
        return new Rational(numerator * other.denominator - other.numerator * denominator,
                denominator * other.denominator);
    }

    public Rational mul(Rational other) {
        return new Rational(numerator * other.numerator, denominator * other.denominator);
    }

    public Rational div(Rational other) {
        return new Rational(numerator * other.denominator, denominator * other.numerator);
    }

    public Rational inv() {
        return new Rational(denominator, numerator);
    }

    public Rational neg() {
        return new Rational(numerator * -1, denominator);
    }

    private void reduce() {

        if (numerator < 0 && denominator < 0) {
            numerator *= -1;
            denominator *= -1;
        }
        int gcf = greatestCommonFactor(numerator, denominator);

        numerator = numerator / gcf;
        denominator = denominator / gcf;
        
        if (denominator < 0) {
            numerator *= -1;
            denominator *= -1;
        }
    }

    @Override
    public String toString() {
        if (denominator == 1) {
            return String.valueOf(numerator);
        }
        if(numerator>denominator){
            return (numerator/denominator) + " " + (numerator % denominator) + "/" + denominator;
        }
        return numerator + "/" + denominator;
    }

    private int greatestCommonFactor(int x, int y) {
        int r = x % y;
        while (r != 0) {
            x = y;
            y = r;
            r = x % y;
        }
        return y;
    }
    
    @Override
    public boolean equals(Object o){
        Rational other = (Rational)o;
        return numerator == other.numerator && denominator==other.denominator;
    }
}
