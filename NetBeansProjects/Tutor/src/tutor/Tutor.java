
package tutor;

import acm.graphics.GLabel;
import acm.program.GraphicsProgram;
import acm.util.RandomGenerator;
import svu.csc213.Dialog;



public class Tutor extends GraphicsProgram{

    private RandomGenerator rg = new RandomGenerator();
    
    @Override
    public void run(){
        int y = 0;
       for(int question=0;question<10;++question){
           Rational r1 = new Rational(rg.nextInt(-4, 4),rg.nextInt(1, 10));
           GRational r1View = new GRational(r1);
           add(r1View,150,100);
           Rational r2 = new Rational(rg.nextInt(1, 4),rg.nextInt(1, 10));
           GRational r2View = new GRational(r2);
           add(r2View,450,100);  
           Rational result;
           switch(rg.nextInt(4)){
               case 0: //addition
                   GLabel plus = new GLabel("+");
                   plus.setFont("Times-40");
                   add(plus,360,250);
                    println("What is" + r1 + " + " + r2 + "?");
                    result = r1.add(r2);
                    break;
               case 1: //subtraction
                   GLabel minus = new GLabel("-");
                   minus.setFont("Times-40");
                   add(minus,360,250);
                    println("What is" + r1 + " - " + r2 + "?");
                    result = r1.sub(r2);
                    break;
               case 2: //multiplication
                   GLabel times = new GLabel("*");
                   times.setFont("Times-40");
                   add(times,360,250);
                    println("What is" + r1 + " * " + r2 + "?");
                    result = r1.mul(r2);
                    break; 
               case 3: 
               default: //division
                   GLabel divide = new GLabel("/");
                   divide.setFont("Times-40");
                   add(divide,360,250);
                    println("What is" + r1 + " + " + r2 + "?");
                    result = r1.div(r2);
                    break;     
           }
           int n = Dialog.getInteger("Enter the numerator of the result: ");
           int d = Dialog.getInteger("Enter the denomninator of the result: ");
           Rational answer = new Rational(n,d);
           if(answer.equals(result)){
               Dialog.showMessage(congratulate());
               y++;
           }else
               Dialog.showMessage("Oh, sorry. That's not right! The correct answer is " + result);
       this.removeAll();
   
       }
       if(y<5){
           if(Dialog.getYesOrNo("You got "+y+" correct. I think you need some more practice. Would you like to try again?")){
               run();
           }
       }
       if(y>5&y<8){
            if(Dialog.getYesOrNo("You got "+y+" correct. You did pretty good. Would you like to try again for a better score?")){
               run();
            }
        }
       if(y>=8){
            if(Dialog.getYesOrNo("You got "+y+" correct. You did amazing! Would you like to try again for more practice?")){
                run();
            }
    }
       //closes the window and exits the program
       System.exit(0);
    }
    
    private String congratulate(){
        switch(rg.nextInt(6)){
            case 0:
                return "Fantastic!";
            case 1:   
                return "Correct!";
            case 2:
                return "Amazing!";
            case 3:
                return "Well done!";
            case 4:
                return "Perfect!";
            case 5:
            default: return "Nice job!";
        }

    }
    
    public static void main(String[] args) {
        new Tutor().start();
    }
    
}
