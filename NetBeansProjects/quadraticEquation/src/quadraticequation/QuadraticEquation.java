/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quadraticequation;
import java.util.Scanner;


public class QuadraticEquation {


    public static void main(String[] args) {
        Scanner myObj = new Scanner(System.in);  // Create a Scanner object
        System.out.println("Quadratic Equation");
    System.out.println("For equations of form Ax^2 + Bx + C");
    
    System.out.println("Enter A:" );
    double a = myObj.nextDouble();  // Read user input
   
    System.out.println("Enter B:");
    double b = myObj.nextDouble();
    
    System.out.println("Enter C:");
    double c = myObj.nextDouble();
    
    double D = (b*b-4*a*c);
    if (D>=0){
        double x1 = (-b + Math.sqrt(D));
        double x2 = (-b - Math.sqrt(D));
        System.out.println("x = " + x1 + "/" + 2*a + ", " + x2 + "/" + 2*a);
    }
    if(D<0){
        double f =(-b);
        double g = (Math.sqrt(4*a*c-b*b));
        System.out.println("x = " + f + "/" + 2*a + " + " + g + "/" + 2*a + "i" + ", " 
                + f + "/" + 2*a + " - (" + g + "/"+ 2*a + ")i" );
    }
    
    }
    
}
