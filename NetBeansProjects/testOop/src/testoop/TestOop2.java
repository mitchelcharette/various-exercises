/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testoop;


public class TestOop2 extends TestOop {
    
    private int myIQ = 135;
    
    @Override
    public void yourIQ(int x, int y){
        System.out.println(x-y);
    }
    
    public void myIQ(){
        System.out.println(myIQ);
    }

    @Override
    public void yourIQ(int x) {
        System.out.println(x);
    }
    public static void main(String[] args) {
        TestOop2 a = new TestOop2();
        a.yourIQ(100);
        a.yourIQ(100, 5);
        a.myIQ();

    }
    
        
}
